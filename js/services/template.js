// Сервис предоставляет функцию, которая возвращает полный путь к вьюхе по относительному пути
angular.module('calculator').factory('template', ['config', function($config) {
  return function(relativePath) {
    return $config.BASE_TEMPLATE_PATH + '/' + relativePath;
  };
}]);