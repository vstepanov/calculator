// 

angular.module('calculator').factory('rateplan', ['common', 'calculator', 'checkboxItem', function($common, $calculator, CheckboxItem) {
  
  function Rateplan() {
    
    Rateplan.superclass.constructor.apply(this, arguments);
    
    var conf = this.getConfiguration();

    function iterator(rateplan) {
      if(rateplan && rateplan.code == this.code) // если код тарифа совпадает, то выделяем его как подключенный
        this.contract();
    }
    
    if(conf.rateplans)
      angular.forEach(conf.rateplans, iterator, this);
    else
      iterator.call(this, conf.rateplan);
    
    
    if(!this.contracted && this.status.toLowerCase() != 'active')
      return this.remove();
    
  }
  
  $common.inherit(Rateplan, CheckboxItem);
  
  Rateplan.prototype.getTooltip = function() {
    var reason = this.getUnchangeableReason(),
        group = this.group.toLowerCase();
    if(this.contracted && !this.isChangeable() && reason)
      return reason;
    if((group == 'inet' || group == 'inetdvbc') && !this.parent.hasSelectedInternet()) {
      return 'Обратите внимание, при заказе подключения услуги Интернет вы можете также заказать «Домашний телефон» и «Wi-Fi роутер», а также опцию «Срочное подключение».';
    }
  };
  
  Rateplan.prototype.isSpecial = function() {
    return this.status.toLowerCase() == 'special';
  };
  
  Rateplan.prototype.isAction = function() {
    return this.status.toLowerCase() == 'action';
  };
  
  Rateplan.prototype.isArchive = function() {
    return this.status.toLowerCase() == 'archive';
  };
  
  Rateplan.prototype.isActive = function() {
    return this.status.toLowerCase() == 'active';
  };
  
  Rateplan.prototype.isActive = function() {
    return this.status.toLowerCase() == 'active';
  };
  
  Rateplan.prototype.getConfiguration = function() {
    var conf = $calculator.getConfiguration();
    switch(this.group.toLowerCase()) {
      case 'inet':
        return conf.internet || {};
        break;
      case 'dvbc':
        return conf.dvbc || {};
        break;
      case 'inetdvbc':
        return conf.internetdvbcpack || {};
    }
  };
  
  return Rateplan;
  
}]);