/*
  Сервис предоставляет key-value хранилище каждому компоненту калькулятора
*/
angular.module('calculator').service('components', function() {
  var components = {};
  
  function Component(componentName) {
    var storage = {}; // key-value хранилище 
    this.name     = componentName;
    this.set      = function(key, value)        { storage[key] = value; };
    this.get      = function(key, defaultValue) { return this.has(key) ? storage[key] : defaultValue; };
    this.has      = function(key)               { return typeof storage[key] != 'undefined'; };
  }
  
  this.get = function(componentName) {
    if(typeof components[componentName] == 'undefined') {
      components[componentName] = new Component(componentName);
    }
    return components[componentName];
  };
});