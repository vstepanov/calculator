// Сервис отвечает за загрузку текущей конфигурации и изменение конфигурации

angular.module('calculator').factory('contract', ['$q', '$http', function($q, $http) {
  
  var data = null;
  
  return {

    /*
      Загрузка списка подключенных услуг
      Формат:
      {
        info: {
          email:
          phone:
        },
        actions: [], - список кодов акций в которых может принять участие
        conf: {
          dvbc:
          internet:
          premium:
          voip:
        }
      }
    */
    load: function() { // загрузка списка подключенных услуг
      var deferred = $q.defer();        
      if(!data) {
        return $http({method: 'GET', url: '/json/getcontract'}).then(function(resolve) {
          if(!data) {
            data = {
              info: resolve.data.contract,
              conf: resolve.data.configuration,
              actions: resolve.data.market_actions
            };
            console.log('CONTRACT:');
            console.log(data);
          }
          return data;
        });
      }
      deferred.resolve(data);
      return deferred.promise;
    },
    
    // изменение списка подключенных услуг
    change: function() {
      
    }
    
  };
  
}]);