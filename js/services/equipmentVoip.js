// 

angular.module('calculator').factory('equipmentVoip', ['common', 'calculator', 'equipment', function($common, $calculator, Equipment) {
  
  function EquipmentVoIP() {
    
    EquipmentVoIP.superclass.constructor.apply(this, arguments);
    
    this.groupName = 'Для телефона';

  }
  
  $common.inherit(EquipmentVoIP, Equipment);
  
  EquipmentVoIP.prototype.canBeSelected = function() {
    return $calculator.getVoIP().selected;
  }
  
  EquipmentVoIP.prototype.getTooltip = function() {
    var tooltip = EquipmentVoIP.superclass.getTooltip.call(this);
    if((!tooltip || this.isGroup) && !this.canBeSelected())
      tooltip = 'Для выбора оборудования телефонии подключите услугу «Домашний телефон».';
    return tooltip;
  };
  
  return EquipmentVoIP;
  
}]);