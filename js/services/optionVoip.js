// 

angular.module('calculator').factory('optionVoip', ['common', 'calculator', 'option', function($common, $calculator, Option) {
  
  function OptionVoIP() {
    
    OptionVoIP.superclass.constructor.apply(this, arguments);
    
  }
  
  $common.inherit(OptionVoIP, Option);
  
  OptionVoIP.prototype.canBeSelected = function() {
    return $calculator.getVoIP().selected;
  }
  
  OptionVoIP.prototype.getTooltip = function() {
    if(!this.canBeSelected())
      return 'Для выбора опции необходимо подключить услугу «Домашний телефон».';
    return OptionVoIP.superclass.getTooltip.call(this);
  };

  return OptionVoIP;
  
}]);