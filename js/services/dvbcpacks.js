// 

angular.module('calculator').factory('dvbcpacks', ['$filter', 'common', 'collection', function($filter, $common, Collection) {
  
  function DVBCPacks(items) {
  
    items = $filter('filter')(items, {status: 'active'});
    
    DVBCPacks.superclass.constructor.call(this, items);
    
  }
  
  $common.inherit(DVBCPacks, Collection);
  
  return DVBCPacks;
  
}]);