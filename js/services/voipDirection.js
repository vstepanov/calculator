// 

angular.module('calculator').factory('voipDirection', ['common', 'calculator', 'checkboxItem', 'voipRateplans', function($common, $calculator, CheckboxItem, VoIPRateplans) {
  
  function VoIPDirection(direction, parent) {
    
    VoIPDirection.superclass.constructor.apply(this, arguments);
    
    if(direction.rateplans)
      this.rateplans = new VoIPRateplans(direction.rateplans, this);
    
  }
  
  $common.inherit(VoIPDirection, CheckboxItem);
  
  VoIPDirection.prototype.contractWasChanged = function() {
    return VoIPDirection.superclass.contractWasChanged.apply(this) || (this.rateplans && this.rateplans.contractWasChanged());
  };

  return VoIPDirection;
  
}]);