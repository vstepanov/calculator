angular.module('calculator').constant('constant', {
  
  CODE_VOIP_BILL:         'voip_bill', // используется в директиве calculatorCheckboxitem
  
  PRICE_TYPE_CREDIT:      'credit',
  PRICE_TYPE_RENT:        'rent',
  PRICE_TYPE_BUY:         'buy'
  
});