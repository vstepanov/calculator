// 

angular.module('calculator').service('calculator', [function() {

  this.getContract = function() {
    return this.contract || {};
  };
  
  this.getRateplans = function() {
    return this.rateplans || [];
  };
  
  this.getInetservices = function() {
    return this.inetservices || [];
  };
  
  this.getDVBCPacks = function() {
    return this.dvbcpacks || [];
  };
  
  this.getOptions = function() {
    return this.options || [];
  };
  
  this.getEquipments = function() {
    return this.equipments || [];
  };
  
  this.getVoIP = function() {
    return this.voip || {};
  };
  
  this.getConfiguration = function() {
    var contract = this.getContract();
    return contract.conf || {};
  };
  
  this.getInfo = function() {
    var contract = this.getContract();
    return contract.info || {};
  };
  
  this.getEmail = function() {
    return this.getInfo().email;
  };
  
  // возвращает true, если пользователь хоть что-то изменил в конфигурации
  this.contractWasChanged = function() {
    var models = [this.rateplans, this.inetservices, this.dvbcpacks, this.options, this.equipments, this.voip];
    for(var i = 0; i<models.length;i++) {
      if(models[i] && typeof models[i].contractWasChanged == 'function' && models[i].contractWasChanged())
        return true;
    }
    return false;
  };
  
}]);