// 

angular.module('calculator').factory('rateplans', ['$filter', 'common', 'calculator', 'confirmation', 'collection', 'rateplan', 'rateplanInternet', 'rateplanDvbc', 'rateplanInternetDvbc',
function($filter, $common, $calculator, $confirmation, Collection, Rateplan, RateplanInternet, RateplanDVBC, RateplanInternetDVBC) {
  
  function Rateplans(items) {
  
    var self = this;
    
    Rateplans.superclass.constructor.call(this, items, {
      'default': Rateplan,
      'group': {
        'inet': RateplanInternet,
        'dvbc': RateplanDVBC,
        'inetdvbc': RateplanInternetDVBC
      }
    });
    
    // возвращает true, если выбран интернет или пакетное предложение
    this.hasSelectedInternet = function() {
      var internetRateplans = $filter('filter')(this.selectedItems, {group: 'INET'});
      return internetRateplans.length ? true : false;
    };
    
    // возвращает true, если выбрана услуга ТВ или пакетное предложение
    this.hasSelectedTV = function() {
      var tvRateplans = $filter('filter')(this.selectedItems, {group: 'DVBC'});
      return tvRateplans.length ? true : false;
    };
    
    this.getSelectedInternetRateplan = function() {
      var internetRateplans = $filter('filter')(this.selectedItems, {group: 'INET'}, true);
      if(internetRateplans.length)
        return internetRateplans[0];
      else
        return null;
    };
    
    this.getContractedInternetRateplan = function() {
      var rateplans = $filter('filter')(this.items, {contracted: true}, true);
      if(rateplans.length)
        return rateplans[0];
      else
        return null;
    };
    
    this.getSelectedDVBCRateplan = function() {
      var rateplans = $filter('filter')(this.selectedItems, {group: 'DVBC'}, true);
      if(rateplans.length)
        return rateplans[0];
      else
        return null;
    };
    
    this.getSelectedDVBCPackRateplan = function() {
      var rateplans = $filter('filter')(this.selectedItems, {group: 'INETDVBC'}, true);
      if(rateplans.length)
        return rateplans[0];
      else
        return null;
    };

  }
  
  $common.inherit(Rateplans, Collection);
  
  Rateplans.prototype.select = function(item) {
    var self = this,
        group = item.group.toLowerCase(),
        optionSMSNotification = $calculator.getOptions().getSMSNotification(),
        rateplan,
        logic1 = function() {
          Rateplans.superclass.deselect.call(self, rateplan); // делаем попытку снять выделение с предыдущего тарифа
          logic2();
        },
        logic2 = function() {
          if(!rateplan || !rateplan.selected) { // если выделение снялось или предыдущего тарифа не было
            Rateplans.superclass.select.call(self, item); // то выделяем новый
            optionSMSNotification.select(); // и заодно выделяем опцию СМС информирование
          }
        };
    switch(group) {
      case 'inet':
        rateplan = this.getSelectedInternetRateplan();
        break;
      case 'dvbc':
        rateplan = this.getSelectedDVBCRateplan();
        break;
      case 'inetdvbc':
        rateplan = this.getSelectedDVBCPackRateplan();
        break;
    }
    if(rateplan && rateplan != item && rateplan.isChangeable()) {
      if(rateplan.contracted && !rateplan.isActive()) { // если подключен активный/специальный/архивный тариф, то требовать подтверждение
        var rateplanType = rateplan.isSpecial() ? 'архивным' : (rateplan.isAction() ? 'акционным' : 'специализированным');
        $confirmation('Тарифный план «'+rateplan.name+'» является '+rateplanType+' тарифом. При смене тарифного плана возврат на него будет невозможен. Продолжить?').
        then(logic1);
      } else
        logic1();
    } else
      logic2();
    
  };
  
  Rateplans.prototype.deselect = function(item) {
    var self = this,
        voip = $calculator.getVoIP(),
        group = item.group.toLowerCase(),
        logic = function() {
          $calculator.getEquipments().getWiFiRouter().deselect();
          Rateplans.superclass.deselect.call(self, item);
        };
        
    switch(group) {
    
      case 'inet':
        if(!this.getContractedInternetRateplan()) {
          if(voip.selected)
            $confirmation('При отказе от подключения услуги Интернет, подключение услуги «Домашний телефон» не возможно. Продолжить?').
            then(function() {
              voip.deselect();
              logic();
            });
          else
            logic();
        }
        break;
        
      default:
        Rateplans.superclass.deselect.call(this, item);
    }
    
    if(!this.selected)
      $calculator.getOptions().getSMSNotification().deselect();
  }
  
  return Rateplans;
  
}]);