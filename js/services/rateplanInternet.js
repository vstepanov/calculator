// 

angular.module('calculator').factory('rateplanInternet', ['common', 'calculator', 'rateplan', function($common, $calculator, Rateplan) {
  
  function RateplanInternet() {
  
    RateplanInternet.superclass.constructor.apply(this, arguments);
    
  }
  
  $common.inherit(RateplanInternet, Rateplan);
  
  return RateplanInternet;
  
}]);