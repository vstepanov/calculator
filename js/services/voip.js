// 

angular.module('calculator').factory('voip', ['common', 'calculator', 'information', 'confirmation', 'checkboxItem', 'voipNumcategories', 'voipDirections',
function($common, $calculator, $information, $confirmation, CheckboxItem, VoIPNumcategories, VoIPDirections) {
  
  function VoIP(numcategories, directions) {
    VoIP.superclass.constructor.call(this, {
      name: 'Домашний телефон',
      description: '46 коп./мин.'
    });
    if(this.getConfiguration().status == 'on')
      this.contract();
      
    this.numcategories = new VoIPNumcategories(numcategories, null, this);
    this.directions = new VoIPDirections(directions, this);
  }
  
  $common.inherit(VoIP, CheckboxItem);
  
  VoIP.prototype.canBeSelected = function() {
    return $calculator.getRateplans().hasSelectedInternet() ? true : false;
  };
  
  VoIP.prototype.getMark = function() {
    if(!this.contracted)
      return 'Новая услуга';
  };
  
  VoIP.prototype.getTooltip = function() {
    if(!$calculator.getRateplans().hasSelectedInternet())
      return 'Для подключения услуги «Домашний телефон» должна быть подключена услуга Интернет. Выберите тариф Интернет или Пакет услуг.';
  };
  
  VoIP.prototype.getConfiguration = function() {
    var conf = $calculator.getConfiguration();
    return conf.voip || {};
  };
  
  VoIP.prototype.select = function() {
    var selectedList = [
      $calculator.getEquipments().getVoIPEquipments()[0].getBy('code')('NKS_VOIP_ATA'), // аренда VoIP шлюза
      $calculator.getOptions().getBy('code')('VOIP_BILL'),                              // опция доставка счета за телефонию
      this.numcategories.getBy('code')('WOOD')                                          // простая категория номера
    ];
    angular.forEach(selectedList, function(item) {
      item.select()
    });
    $information(
      'Обратите внимание, при заказе подключения услуги «Домашний телефон» автоматически подключено:',
      selectedList,
      'Вы можете заказать подключение внутризоновой, международной и междугородной связи, а также опцию «Срочное подключение».'
    );
    VoIP.superclass.select.call(this, arguments);
  };
  
  VoIP.prototype.deselect = function() {
    var self = this;
    if(!this.contracted) {
      $confirmation('Вы действительно хотите отказаться от подключения услуги «Домашний телефон»?').
      then(function() {
        VoIP.superclass.deselect.call(self, arguments);
        $information(
          'Обратите внимание, при отказе от подключения услуги «Домашний телефон», также выполнен отказ от:',
          self.batchDeselect(
            [].concat(
              $calculator.getOptions().getVoIPOptions(),
              $calculator.getEquipments().getVoIPEquipments(),
              self.numcategories.selectedItems
            )
          )
        );
      });
    } else
      $information('Отключение услуги «Домашний телефон» производится в офисе продаж и обслуживания ОнЛайм при сдаче оборудования.');
  };
  
  VoIP.prototype.contractWasChanged = function() {
    return VoIP.superclass.contractWasChanged.apply(this) || this.numcategories.contractWasChanged() || this.directions.contractWasChanged();
  };
  
  return VoIP;
  
}]);