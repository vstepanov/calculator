// 

angular.module('calculator').factory('voipRateplan', ['common', 'calculator', 'checkboxItem', function($common, $calculator, CheckboxItem) {
  
  function VoIPRateplan() {
    
    VoIPRateplan.superclass.constructor.apply(this, arguments);
    
  }
  
  $common.inherit(VoIPRateplan, CheckboxItem);

  return VoIPRateplan;
  
}]);