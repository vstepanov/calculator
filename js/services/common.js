/*
  Сервис предоставляет общие полезные функции
*/
angular.module('calculator').factory('common', ['$filter', function($filter) {

  // Используется в multipleFilter
  function deepCheckValueInList(list, value, inside) {
    inside = typeof inside == 'undefined' ? 0 : inside+1;
    if(inside > 10)
      throw 'Exceeded max deep 10 in deepCheckValueInList('+list+', '+value+')'; // оставляем тут throw т.к. это фатальная ошибка, которая может повесить браузер
    for(var i=0;i<list.length;i++) {
      if(angular.isArray(list[i]) && deepCheckValueInList(list[i], value, inside) || list[i] == value)
        return true;
      else if(list[i] == value)
        return true;
    }
    return false;
  }
  
  return {
  
    /**
     * Возвращает единицу измерения с правильным окончанием
     * 
     * @param {Number} num      Число
     * @param {Object} cases    Варианты слова {nom: 'час', gen: 'часа', plu: 'часов'}
     * @return {String}            
     */
    units: function(num, cases) {
      num = Math.abs(num);
      
      var word = '';
      
      if (num.toString().indexOf('.') > -1) {
          word = cases.gen;
      } else { 
          word = (
              num % 10 == 1 && num % 100 != 11 
                  ? cases.nom
                  : num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) 
                      ? cases.gen
                      : cases.plu
          );
      }
      
      return word;
    },
    
    /*
      Аналогично $filter('filter'), но позволяет в качестве параметра expression передавать структуру вида:
        {key1: value, key2: [value1, value2]}
      тогда key2 должен соответствовать хотя бы одному из значений value1 или value2
    */
    multipleFilter: function(values, pattern) {
      return $filter('filter')(values, function(value) {
        var result = true;
        angular.forEach(pattern, function(patternValue, key) {
          if(typeof value[key] == 'undefined')
            result = false;
          else if(typeof patternValue == 'undefined')
            result = true;
          else {
            var val = value[key].toLowerCase()
            result = result && (angular.isArray(patternValue) ? deepCheckValueInList(patternValue, val) : val == patternValue);
          }
        });
        return result;
      });
    },
    
    // Функция для наследования. Взято отсюда: http://javascript.ru/tutorial/object/inheritance#nasledovanie-na-klassah-funkciya-extend
    inherit: function(Child, Parent) {
    	var F = function() { }
    	F.prototype = Parent.prototype
    	Child.prototype = new F()
    	Child.prototype.constructor = Child
    	Child.superclass = Parent.prototype
    }
    
  };
}]);