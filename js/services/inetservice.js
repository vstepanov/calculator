// 

angular.module('calculator').factory('inetservice', ['common', 'calculator', 'checkboxItem', function($common, $calculator, CheckboxItem) {
  
  function Inetservice() {
    
    Inetservice.superclass.constructor.apply(this, arguments);
    
  }
  
  $common.inherit(Inetservice, CheckboxItem);
  
  Inetservice.prototype.isInvisible = function() {
    if(this.code && this.code.toLowerCase() == 'nks_filtr_conf') // Услуга «Фильтрация трафика» отображается только если подключена у Абонента
      return !this.selected;
  };
  
  Inetservice.prototype.tooltipType = 'html';
    
  Inetservice.prototype.getTooltip = function() {
    var titleName = 'Дополнительные услуги',
        url = '';

    switch(this.group.toLowerCase()) {
      case 'antivirus':
        url = '/antivir';
        break;
      case 'upto100':
        url = '/upto100';
        break;
      case 'turbo':
        url = '/turbo';
        break;
      case 'parental':
        url = '/parental';
        break;
      case 'internal':
        url = '/internal';
        break;
      case 'static':
        url = '/ip/static';
        break;
    }
    if($calculator.getRateplans().hasSelectedInternet())
      return 'Управление услугой в разделе «'+titleName+'». <a href="'+url+'" target="_blank">Перейти</a>';
    else
      return 'Управление услугой в разделе «'+titleName+'». Для подключения услуги необходимо подключить услугу Интернет. <a href="'+url+'" target="_blank">Перейти</a>';
  };
  
  // Абонент не может выполнять подключение \ отключение дополнительных услуг в Калькуляторе
  Inetservice.prototype.disabled = true;
  
  return Inetservice;
  
}]);