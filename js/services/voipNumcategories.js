// 

angular.module('calculator').factory('voipNumcategories', ['common', 'calculator', 'collection', function($common, $calculator, Collection) {
  
  function VoIPNumcategories() {
    
    VoIPNumcategories.superclass.constructor.apply(this, arguments);
    
  }
  
  $common.inherit(VoIPNumcategories, Collection);
  
  VoIPNumcategories.prototype.select = function(item) {
    if(this.selectedItems.length > 0 && this.selectedItems[0] != item) // запрещаем выбирать более одного элемента
      this.selectedItems[0].deselect();
    VoIPNumcategories.superclass.select.apply(this, arguments);
  };

  return VoIPNumcategories;
  
}]);