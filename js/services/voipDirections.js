// 

angular.module('calculator').factory('voipDirections', ['common', 'calculator', 'collection', 'voipDirection',
function($common, $calculator, Collection, VoIPDirection) {
  
  function VoIPDirections(directions, parent) {
    
    VoIPDirections.superclass.constructor.call(this, directions, VoIPDirection, parent);
    
  }
  
  $common.inherit(VoIPDirections, Collection);

  return VoIPDirections;
  
}]);