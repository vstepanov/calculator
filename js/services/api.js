/*
  Здесь API взаимодействия с веб сервисами и манипуляция с данными
*/
angular.module('calculator').factory('api', ['$q', 'catalog', 'contract', 'calculator', 'rateplans', 'inetservices', 'dvbcpacks', 'options', 'equipments', 'voip', function($q, $catalog, $contract, $calculator, Rateplans, Inetservices, DVBCPacks, Options, Equipments, VoIP) {

  var loaded = false,
      loadInProgress = false,
      deferredList = [];
      
  function initCalculator(catalog, contract) {
    if(contract)
      $calculator.contract = contract;
    
    $calculator.rateplans    = new Rateplans(catalog.rateplans);
    $calculator.inetservices = new Inetservices(catalog.inetservices);
    $calculator.dvbcpacks    = new DVBCPacks(catalog.dvbcpacks);
    $calculator.options      = new Options(catalog.options);
    $calculator.equipments   = new Equipments(catalog.equipments);
    $calculator.voip         = new VoIP(catalog.numcategories, catalog.voip);
    
    
    console.log($calculator);
  }
  
  return {
  
    load: function(options) {
      var deferred = $q.defer();        
      if(!loaded) {
        deferredList.push(deferred);
        if(!loadInProgress) {
          loadInProgress = true;
          var promises = [$catalog.load()];
          if(typeof options.contract == 'undefined' || options.contract)
            promises.push($contract.load());
          $q.all(promises).then(function(results) {
            console.log('load complete');
            loaded = true;
            initCalculator(results[0], results[1]);
            for(var i=0;i<deferredList.length;i++) {
              deferredList[i].resolve($calculator);
            }
          });
        }
        return deferred.promise;
      }
      deferred.resolve($calculator);
      return deferred.promise;
    }
    
  };
  
}]);