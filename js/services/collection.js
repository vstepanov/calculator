// Сервис предоставляет базовую модель коллекции, содержащей другие элементы

angular.module('calculator').factory('collection', ['common', 'checkboxItem', function($common, CheckboxItem) {
  
  function Collection(items, factory, parent) {
    
    if(items.length == 0)
      throw 'Not allowed to create empty collection';
    
    var self = this;
    
    this.parent = parent;
    this.factory = factory || CheckboxItem; // метод с помощью которого создаются дочерние элементы
    this.parent = parent; // сохраняем ссылку на родительский объект
    this.selectedItems = []; // здесь хранится список выделенных элементов
    this.items = [];
    angular.forEach(items, function(item) {
      var factory = self.getFactoryForItem(item),
          newitem = new factory(item, self);
      if(!newitem.removed) {
        self.items.push(newitem);
        if(newitem.selected)
          self.selectedItems.push(newitem);
      }
    });
    
  }
  
  // коллекция наследуется от CheckboxItem т.к. имеет много общего с ним
  $common.inherit(Collection, CheckboxItem);
  
  Collection.prototype.isCollection = true; // ставим признак того, что это коллекция
  Collection.prototype.groupedBy = null; // здесь будем хранить поле, по которому сгруппировано
  
   /*
    Функция для группировки элементов
    groupBy         - поле по которому группировать
    groupForOneItem - создавать группу для одного элемента или нет true|false
  */
  Collection.prototype.groupBy = function(groupBy, groupForOneItem) {
    var self = this,
        groups = {}, // здесь будем хранить кол-во элементов в каждой группе
        items = self.items;
    
    self.items = []; // заново инициализируем список элементов
    
    self.groupedBy = groupBy;
    
    // сначала формируем список групп
    angular.forEach(items, function(item) {
      if(typeof groups[item[groupBy]] == 'undefined')
        groups[item[groupBy]] = [];
      groups[item[groupBy]].push(item);
    });
    
    // теперь создаем сами группы
    angular.forEach(groups, function(items, group) {
    
      if(items.length > 1 || groupForOneItem) {
        var factory = self.getFactoryForItem(items[0]),
            checkboxItemGroup = new factory(items, self);
        checkboxItemGroup[groupBy] = group; // присваиваем значение поля группировки
        self.items.push(checkboxItemGroup);
      } else {
        self.items.push(items[0]);
      }
    });

  };
  
  Collection.prototype.select = function(item) {
    this.selected = true;
    item.selected = true;
    if(this.selectedItems.indexOf(item) == -1)
      this.selectedItems.push(item);
  };
  
  Collection.prototype.deselect = function(item) {
    this.selected = false;
    if(item) {
      item.selected = false;
      if(this.selectedItems.indexOf(item) != -1)
        this.selectedItems.splice(this.selectedItems.indexOf(item), 1);
    }
  };
  
  Collection.prototype.disable = function() {
    this.disabled = true;
    angular.forEach(this.items, function(item) {
      item.disabled = true;
    });
  };
  
  Collection.prototype.enable = function() {
    this.disabled = false;
    angular.forEach(this.items, function(item) {
      item.disabled = false;
    });
  };
  
  Collection.prototype.each = function(callback) {
    angular.forEach(this.items, function(item) {
      (callback || angular.noop)(item);
      if(item.isCollection) {
        item.each(callback);
      }
    });
  };
  
  /*
    Функция возвращает фабрику для элемента на основе конфигурации хранящейся в свойстве factory
    Свойство factory должно содержать конкретную фабрику или иметь формат:
    {
      default: defaultFactory,
      field1: {
        value1: factory1,
        value2: factory2,
        ...
      },
      field2: {
        value3: factory3,
        value4: factory4,
        ...
      },
      ...
    }
  */
  Collection.prototype.getFactoryForItem = function(item) {
    if(typeof this.factory == 'function')
      return this.factory;
    var factory,
        defaultFactory = this.factory['default'];
    angular.forEach(this.factory, function(factories, field) {
      if(factories && item[field] && factories[item[field].toLowerCase()])
        factory = factories[item[field].toLowerCase()];
    });
    if(factory)
      return factory;
    else if(defaultFactory)
      return defaultFactory;
    else
      throw 'No factory matches for item ['+angular.toJson(item)+']';
  };
  
  Collection.prototype.contractWasChanged = function() {
    for(var i=0;i<this.items.length;i++) {
      if(this.items[i].contractWasChanged())
        return true;
    }
    return false;
  };
  
  return Collection;
  
}]);