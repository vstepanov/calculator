// Показывает всплывающее информационное окно

angular.module('calculator').factory('information', ['template', 'ngDialog', function($template, ngDialog) {
  
  return function() {
    var html = '';
    angular.forEach(arguments, function(arg) {
      if(angular.isArray(arg)) {
        html += '<ul>';
        angular.forEach(arg, function(item) {
          html += '<li>'+item.getName()+'</li>';
        });
        html += '</ul>';
      } else {
        html += arg;
      }
    });
    ngDialog.open({ template: '<div class="calculator-dialog"><p>'+html+'</p><br><button ng-click="closeThisDialog()">OK</button></div>', plain: true });
  };
  
}]);