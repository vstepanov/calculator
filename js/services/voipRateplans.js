// 

angular.module('calculator').factory('voipRateplans', ['common', 'calculator', 'collection', 'voipRateplan',
function($common, $calculator, Collection, VoIPRateplan) {
  
  function VoIPRateplans(rateplans, parent) {
    
    VoIPRateplans.superclass.constructor.call(this, rateplans, VoIPRateplan, parent);
    
  }
  
  $common.inherit(VoIPRateplans, Collection);

  return VoIPRateplans;
  
}]);