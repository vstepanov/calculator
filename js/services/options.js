// 

angular.module('calculator').factory('options', ['$filter', 'common', 'collection', 'option', 'optionVoip', function($filter, $common, Collection, Option, OptionVoIP) {
  
  function Options(items) {
  
    items = $filter('filter')(items, {status: 'active'});
    
    Options.superclass.constructor.call(this, items, {
      'default': Option,
      'group': {
        'voip': OptionVoIP
      }
    });
    
    this.getSMSNotification = function() {
      return $filter('filter')(this.items, {code: 'nks_sms_conf'})[0];
    };
    
    this.getVoIPOptions = function() {
      return this.getBy('group')('voip', true);
    };
    
  }
  
  $common.inherit(Options, Collection);
  
  return Options;
  
}]);