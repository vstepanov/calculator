// Сервис предоставляет функции для централизованого добавления ошибок калькулятора

angular.module('calculator').factory('error', [function() {
  
  var errors = []; // тут будем хранить список ошибок возникающих в процессе выполнения, но которые не будем генерировать с помощью throw, чтобы полностью не прекращать работоспособность калькулятора
  
  return {
    add: function(message) {
      console.log('ERROR: ' + message);
      errors.push({message: message});
    }
  };
  
}]);