// 

angular.module('calculator').factory('equipmentWifi', ['common', 'calculator', 'equipment', function($common, $calculator, Equipment) {
  
  function EquipmentWiFi() {
    
    EquipmentWiFi.superclass.constructor.apply(this, arguments);
    
    this.groupName = 'Wi-Fi роутер';
    
  }
  
  $common.inherit(EquipmentWiFi, Equipment);
  
  EquipmentWiFi.prototype.canBeSelected = function() {
    return $calculator.getRateplans().hasSelectedInternet();
  };
  
  EquipmentWiFi.prototype.getTooltip = function() {
    if(this.isGroup && !this.canBeSelected()) {
      this.tooltipPosition = 'top';
      return 'Для подключения «Wi-Fi роутера» должна быть подключена услуга Интернет. Выберите тариф Интернет или Пакет услуг.';
    }
    return EquipmentWiFi.superclass.getTooltip.call(this);
  };
  
  return EquipmentWiFi;
  
}]);