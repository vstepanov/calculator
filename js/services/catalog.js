// Сервис отвечает за загрузку продуктового каталога

angular.module('calculator').factory('catalog', ['$q', '$http', function($q, $http) {
  
  var data = null;
  
  return {
  
    // загрузка каталога
    load: function() {
      var deferred = $q.defer();        
      if(!data) {
        return $http({method: 'GET', url: '/json/catalogue'}).then(function(resolve) {
          if(!data) {
            console.log('CATALOG:');
            data = resolve.data;
            console.log(data);
          }
          return data;
        });
      }
      deferred.resolve(data);
      return deferred.promise;
    }
    
  };
  
}]);