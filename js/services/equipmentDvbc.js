// 

angular.module('calculator').factory('equipmentDvbc', ['common', 'calculator', 'equipment', function($common, $calculator, Equipment) {
  
  function EquipmentDVBC() {
    
    EquipmentDVBC.superclass.constructor.apply(this, arguments);
    
    this.groupName = 'Для первого ТВ';
    
    this.canBeSelected = function() {
      return $calculator.getRateplans().hasSelectedTV();
    }

  }
  
  $common.inherit(EquipmentDVBC, Equipment);
  
  return EquipmentDVBC;
  
}]);