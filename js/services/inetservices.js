// Доп услуги Интернет должны быть сгруппированны по полю group

angular.module('calculator').factory('inetservices', ['$filter', 'common', 'calculator', 'collection', 'inetservice', function($filter, $common, $calculator, Collection, Inetservice) {
  
  function Inetservices(items) {
    
    items = $filter('filter')(items, {status: 'active'});
    
    Inetservices.superclass.constructor.call(this, items, Inetservice);
    
    this.groupBy('group');
    
    var getByGroup = this.getBy('group');

    getByGroup('turbo').groupName = 'Турбокнопка';
    getByGroup('upto100').groupName = 'Догони до 100';
    getByGroup('antivirus').groupName = 'Антивирус';
  
  }
  
  $common.inherit(Inetservices, Collection);
  
  return Inetservices;
  
}]);