// 

angular.module('calculator').factory('rateplanInternetDvbc', ['common', 'calculator', 'rateplan', function($common, $calculator, Rateplan) {
  
  function RateplanInternetDVBC() {
    
    RateplanInternetDVBC.superclass.constructor.apply(this, arguments);
    
  }
  
  $common.inherit(RateplanInternetDVBC, Rateplan);
  
  return RateplanInternetDVBC;
  
}]);