// Сервис предоставляет базовую модель объекта, который может быть выбран для заказа

angular.module('calculator').factory('checkboxItem', ['$filter', 'price', function($filter, PriceModel) {
  
  function CheckboxItem(item, parent) {
    var self = this;
    if(angular.isArray(item)) {
      self.isGroup = true;
      self.items = [];
      angular.forEach(item, function(item) {
        var item = item instanceof self.constructor ? item : new self.constructor(item);
        item.parent = self;
        self.items.push(item);
        if(!self.price || item.price.value < self.price.value) // для группы определяем минимальную цену среди ее элементов
          self.price = item.price;
      });
      self.disabled = self.subitemsDisabled();
    } else {
      angular.extend(this, item); // расширяем текущий объект свойствами item'а
    }
    this.parent = parent; // сохраняем ссылку на родительский объект (коллекцию или группу)
    this.price = new PriceModel(item.price || {}); // цену инициализируем отдельной моделью 
  };
  
  CheckboxItem.prototype.removed = false; // это свойство устанавливается в методе removed, на основе его значения этот элемент не будет добавлен в коллекцию
  
  CheckboxItem.prototype.selected = false; // по-умолчанию услуга не выбрана
  
  CheckboxItem.prototype.disabled = false; // по-умолчанию услуга может быть выбрана
  
  CheckboxItem.prototype.contracted = false; // по-умолчанию услуга не подключена
  
  CheckboxItem.prototype.groupName = null; // имя группы
  
  CheckboxItem.prototype.isGroup = false; // по-умолчанию услуга не является группой и не содержит дочерних элементов
  
  CheckboxItem.prototype.items = null; // по-умолчанию услуга не имеет дочерних элементов
  
  CheckboxItem.prototype.selectedItem = null; // для элементов содержащих дочерние элементы (при группировке), по-умолчанию дочерний элемент не выбран

  CheckboxItem.prototype.tooltipType = 'simple'; // по-умолчанию простой тип подсказки
  
  CheckboxItem.prototype.tooltipPosition = 'top'; // по-умолчанию позиция подсказки вверху
  
  CheckboxItem.prototype.isInvisible = function() {
    return false; // по-умолчанию услуга отображается
  };
  
  CheckboxItem.prototype.select = function(subitem) {
    if(this.parent)
      this.parent.select(this);
    else
      this.selected = true;
    
    if(subitem && this.selected) {
      if(this.selectedItem && this.selectedItem != subitem)
        this.selectedItem.selected = false;
      subitem.selected = true;
      this.selectedItem = subitem;
    }
  };
  
  CheckboxItem.prototype.deselect = function() {
    if(this.parent)
      this.parent.deselect(this);
    else
      this.selected = false;
      
    if(this.isGroup && !this.selected && this.selectedItem) {
      this.selectedItem.selected = false;
      this.selectedItem = null;
    }
  };
  
  CheckboxItem.prototype.disable = function() {
    this.disabled = true;
    if(this.parent && this.parent.isGroup && this.parent.subitemsDisabled())
      this.parent.disabled = true;
  };
  
  CheckboxItem.prototype.enable = function() {
    this.disabled = false;
    if(this.parent && !this.parent.isGroup && this.parent.subitemsDisabled())
      this.parent.disabled = false;
  };
  
  CheckboxItem.prototype.canBeSelected = function() {
    return this.disabled ? false : true;
  };
  
  CheckboxItem.prototype.getMark = function() {
    return null;
  };
  
  CheckboxItem.prototype.getMarkRef = function() {
    return null;
  };
  
  CheckboxItem.prototype.getTooltip = function() {
    return false; // по-умолчанию не показывать подсказку
  };
  
  CheckboxItem.prototype.subitemsDisabled = function() {
    var disabled = true;
    angular.forEach(this.items, function(subitem) {
      disabled = disabled && subitem.disabled;
    });
    return disabled;
  };
  
  // удаляет элемент из коллекции
  CheckboxItem.prototype.remove = function() {
    this.removed = true;
    if(this.parent)
      this.parent.remove(this);
  };
  
  // пометить услугу подключенной у абонента (используется при инициализации)
  CheckboxItem.prototype.contract = function() {
    this.contracted = true;
    this.selected = true;
  };
  
  // возвращает true, если состояние опции было изменено по отношению к действующему контакту
  CheckboxItem.prototype.contractWasChanged = function() {
    return this.contracted && !this.selected || !this.contracted && this.selected;
  };
  
  // этот метод должен быть переопределен в дочернем классе
  CheckboxItem.prototype.getConfiguration = function() {
    return {};
  };
  
  CheckboxItem.prototype.isChangeable = function() {
    return this.getConfiguration().changeable;
  };
  
  CheckboxItem.prototype.getUnchangeableReason = function() {
    return this.getConfiguration().reason;
  };
  
  /*
    Выполняет отключение услуг из списка и возвращает список услуг которые реально были отключены
  */
  CheckboxItem.prototype.batchDeselect = function(items) {
    var deselectedList = [];
    angular.forEach(items, function(item) {
      if(item.selected) {
        var deselectedItem = item.isGroup ? item.selectedItem : item;
        item.deselect();
        if(!item.selected)
          deselectedList.push(deselectedItem);
      }
    });
    return deselectedList;
  }
  
  // возвращает функцию возвращающую элемент или список элементов коллекции по заданному полю
  CheckboxItem.prototype.getBy = function(field) {
    var self = this;
    return function(value, alwaysReturnArray) {
      var items = [];
      value = value.toLowerCase();
      for(var i=0;i<(self.items || []).length;i++) {
        if(self.items[i][field] && self.items[i][field].toLowerCase() == value)
          items.push(self.items[i]);
      }
      if(items.length == 0)
        return alwaysReturnArray ? [] : null;
      if(items.length == 1)
        return alwaysReturnArray ? items : items[0];
      else
        return items;
    };
  };
  
  CheckboxItem.prototype.getName = function() {
    return this.isGroup ? this.groupName : this.name;
  };
  
  CheckboxItem.prototype.add = function(item) {
    this.isGroup = true;
    if(!this.items)
      this.items = [];
    this.items.push();
  };
  
  return CheckboxItem;
  
}]);