// 

angular.module('calculator').factory('equipment', ['common', 'calculator', 'checkboxItem', function($common, $calculator, CheckboxItem) {
  
  function Equipment() {
    
    Equipment.superclass.constructor.apply(this, arguments);

  }
  
  $common.inherit(Equipment, CheckboxItem);
  
  Equipment.prototype.getTooltip = function() {
    return this.description;
  };
  
  Equipment.prototype.tooltipPosition = 'left';
  
  return Equipment;
  
}]);