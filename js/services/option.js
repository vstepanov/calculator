// 

angular.module('calculator').factory('option', ['common', 'calculator', 'checkboxItem', function($common, $calculator, CheckboxItem) {
  
  function Option() {
    
    Option.superclass.constructor.apply(this, arguments);
    
  }
  
  $common.inherit(Option, CheckboxItem);
  
  Option.prototype.getTooltip = function() {
    return this.description;
  };
  
  Option.prototype.isInvisible = function() {
    switch(this.code.toLowerCase()) {
      case 'nks_quick_connection':
        return !$calculator.getRateplans().hasSelectedInternet(); // услуга Срочное подключение отображается только если выбран или подключен Интернет
      case 'voip_antiaon':
      case 'voip_redirect':
        return !$calculator.getVoIP().selected; // услуги Анти АОН и переадресация вызова отображаются только если выбрана услуга VoIP
    }
  };
  
  return Option;
  
}]);