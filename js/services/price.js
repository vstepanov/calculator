// Сервис предоставляет модель цены

angular.module('calculator').factory('price', ['error', 'constant', function($error, $constant) {
  
  function Price(price) {
    if(!price) {
      $error.add('Error instanciate Price model with empty price');
      price = {value: 0}; // чтобы не разрушать дальнейшую работу в случае ошибки, подсунем корректную стркутуру цены
    }
    this.value         = price.value || 0;
    this.period        = (typeof price.period        != 'undefined' ? price.period        : 1);
    this.length        = (typeof price.length        != 'undefined' ? price.length        : 0);
    this.delay         = (typeof price.delay         != 'undefined' ? price.delay         : 0);
    this.connectionfee = (typeof price.connectionfee != 'undefined' ? price.connectionfee : 0);
    this.deliveryfee   = (typeof price.deliveryfee   != 'undefined' ? price.deliveryfee   : 0);
    
    /*
      Устанавливаем тип цены
        rent     - аренда:    period  > 0 && length == 0
        credit   - рассрочка: period  > 0 && length  > 0
        buy      - покупка:   period == 0 && length == 0
    */
    if(this.period > 0 && this.length == 0)
      this.type = $constant.PRICE_TYPE_RENT;
    else if(this.period > 0 && this.length > 0)
      this.type = $constant.PRICE_TYPE_CREDIT;
    if(this.period == 0 && this.length == 0)
      this.type = $constant.PRICE_TYPE_BUY;
    
    // устанавливаем период рассрочки в годах
    this.credit_years = this.period * this.length / 12;
  };
  
  return Price;
  
}]);