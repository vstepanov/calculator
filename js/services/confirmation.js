// Показывает всплывающее окно подтверждения

angular.module('calculator').factory('confirmation', ['ngDialog', function(ngDialog) {
  
  return function(text) {
    return ngDialog.openConfirm({ template: '<div class="calculator-dialog"><p>'+text+'</p><br/><div><button ng-click="confirm()">Да</button> <button ng-click="closeThisDialog()">Нет</button></div></div>', plain: true });
  };
  
}]);