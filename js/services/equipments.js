// 

angular.module('calculator').factory('equipments', ['$filter', 'common', 'collection', 'equipmentVoip', 'equipmentDvbc', 'equipmentWifi',
function($filter, $common, Collection, EquipmentVoIP, EquipmentDVBC, EquipmentWiFi) {
  
  function Equipments(items) {
  
    var getByGroup = this.getBy('group'),
        factories = {
          'group': {
            'voip': EquipmentVoIP,
            'dvbc': EquipmentDVBC,
            'inet': EquipmentWiFi
          }
        };
  
    items = $filter('filter')(items, {status: 'active'});
    
    Equipments.superclass.constructor.call(this, items, factories);
    
    this.groupBy('group', true);
    
    this.getWiFiRouter = function() {
      return getByGroup('inet');
    };
    
    this.getVoIPEquipments = function() {
      return getByGroup('voip', true);
    };
    
  }
  
  $common.inherit(Equipments, Collection);
  
  return Equipments;
  
}]);