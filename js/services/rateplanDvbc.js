// 

angular.module('calculator').factory('rateplanDvbc', ['common', 'calculator', 'rateplan', function($common, $calculator, Rateplan) {
  
  function RateplanDVBC() {
    
    RateplanDVBC.superclass.constructor.apply(this, arguments);

  }
  
  $common.inherit(RateplanDVBC, Rateplan);
  
  return RateplanDVBC;
  
}]);