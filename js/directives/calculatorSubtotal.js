/*
  Это специальная директива для расчета и отображения итоговой суммы.
  В отличие от других компонент она не основана на директиве calculatorBlock.
*/
angular.module('calculator').directive('calculatorSubtotal', ['$state', 'template', 'calculator', function($state, $template, $calculator) {
  return {
    restrict: 'A',
    templateUrl: $template('calculator-subtotal.html'),
    link: function($scope, elm, attrs) {
      elm.addClass('calculator-subtotal');
      $scope.$state = $state;
      $scope.$calculator = $calculator;
    }
  };
}]);