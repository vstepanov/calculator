/*
  
*/
angular.module('calculator').directive('calculatorActivation', ['$state', 'template', function($state, $template) {
  return {
    restrict: 'A',
    templateUrl: $template('calculator-activation.html'),
    link: function($scope, elm, attrs) {
      elm.addClass('calculator-activation');
    }
  };
}]);