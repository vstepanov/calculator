angular.module('calculator').directive('calculatorCheckboxitem', ['$compile', '$filter', 'constant', 'template', 'calculator', 'dvbcpacks', function($compile, $filter, $constant, $template, $calculator, DVBCPacks) {

  /*
    Группировка элементов по цене
    Надо разбить элементы на группы: Аренда, Рассрочка на N лет, Купить
    
    Элементы структуры price участвующие в группировке:
      { period, length }
      
    Сценарии:
      1. Аренда:             { period: >0,  length: 0  }
      2. Рассрочка на N лет: { period: >0,  length: >0 } где N = period * length / 12
      3. Купить:             { period: 0,   length: 0  }
      
    Возвращает список групп, каждая группа в свою очередь представляет собой список элементов
    Чтобы узнать параметры цены группы надо просто взять первый элемент из группы и посмотреть его структуру price
  */
  function groupItemsByPrice(items) {
    var groups = {},
        list = [];
    angular.forEach(items, function(item) {
      var priceType = item.price.type+item.price.credit_years;
      if(!groups[priceType])
        groups[priceType] = [];
      groups[priceType].push(item);
    });
    angular.forEach(groups, function(group) {
      list.push(group);
    });
    return list;
  }
      
  return {
    restrict: 'A',
    scope: {
      item: '=calculatorCheckboxitem' // Если передана коллекция, то будет показан выпадающий список
    },
    template: '<div class="checkboxitem-inner {{className}}" calculator-tooltip="item.getTooltip()" tooltip-type="{{item.tooltipType}}" tooltip-position="top" ng-include="templateUrl" ng-click="clickInner()" ng-class="{active:item.selected, disabled: !item.canBeSelected()}" ng-show="!item.isInvisible()"></div>',
    link: function($scope, elm, attrs) {
    
      // внутренний template
      var template;
      
      elm.addClass('checkboxitem');
      
      // имя дополнительного CSS-класса у .checkboxitem-inner, может быть либо group либо item
      $scope.className = $scope.item.isGroup ? 'group' : 'item';
      
      // выбираем внутренний template по определенным условиям
      if($scope.item.isGroup) {
        template = 'group';
        $scope.itemsByPrice = groupItemsByPrice($scope.item.items);
        // если группа получилась одна и это покупка, то не группировать
        if($scope.itemsByPrice.length == 1 && $scope.itemsByPrice[0][0].price.type == $constant.PRICE_TYPE_BUY) {
          $scope.itemsByPrice = null;
          $scope.subitems = $scope.item.items;
        }
      } else if($scope.item.code.toLowerCase() == $constant.CODE_VOIP_BILL) {
        template = 'voipbill';
        $scope.email = $calculator.getEmail();
        $scope.className += ' voipbill';
      } else if($scope.item.parent instanceof DVBCPacks)
        template = 'dvbcitem'; // для DVBC особый шаблон
      else
        template = 'item';
      
      $scope.templateUrl = '/calculator/views/checkboxitem/'+template+'.html';
      
      // клик по выпадающему списку (элемент со стрелочкой вниз) для коллекции
      $scope.clickGroup = function() {
        elm.find('input[type="checkbox"]').prop('checked', $scope.item.selected ? true : false);
        elm.prepend($compile('<div ng-include="\''+$template('checkboxitem/popup.html')+'\'" class="checkboxitem-popup-wrapper"></div>')($scope));
      };

      // клик по групповому чекбоксу
      $scope.clickGroupCheckbox = function() {
        var selected = !$scope.item.selected; // потому что событие срабатывает до того как обновляется модель, поэтому фактически selected = !selected
        if(!selected)
          $scope.item.deselect();
        else
          $scope.clickGroup();
      }
      
      // закрытие всплывающего окна, повешено на клик по подложке
      $scope.closePopup = function() {
        elm.find('.checkboxitem-popup-wrapper').remove();
      };
      
      // клик по checkboxitem-inner для dvbcitem
      if(template == 'dvbcitem') {
        $scope.clickInner = function() {
          var selected = !$scope.item.selected;
          if(selected)
            $scope.item.select();
          if(!selected) {
            $scope.item.deselect();
            elm.find('.checkboxitem-inner').addClass('inactive');
            elm.one('mouseout', function() {
              elm.find('.checkboxitem-inner').removeClass('inactive');
            });
          }
        };
      }
      
    }
  };
}]);