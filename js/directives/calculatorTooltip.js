angular.module('calculator').directive('calculatorTooltip', [function() {

  function simpleTooltip($scope, elm) {
    var position = $scope.position ? $scope.position : 'top';
    $scope.$watch('tooltip', function(value) {
      if(value) {
        elm.addClass('hint--'+position);
        elm.attr('data-hint', value);
      } else {
        elm.removeClass('hint--'+position);
        elm.removeAttr('data-hint');
      }
    });
  }
  
  function htmlTooltip($scope, elm) {
    var tooltipElm = $('#calculatorTooltip');
    elm.bind('mouseover', function() {
      if(!elm[0].tooltip || tooltipElm[0].target == elm[0]) {
        return;
      }
      tooltipElm.find('.tooltip-inner').html(elm[0].tooltip);
      tooltipElm[0].target = elm[0];
      var pos = elm.offset(),
          height = tooltipElm.outerHeight();
      tooltipElm.show();
      /*if(tooltipElm[0].animationInProgress) {
        tooltipElm.stop();
        tooltipElm.css({opacity:0});
        tooltipElm.show();
        tooltipElm[0].animationInProgress = false;
      } else {
        tooltipElm[0].animationInProgress = true;
        tooltipElm.fadeIn({
          complete: function() {
            tooltipElm[0].animationInProgress = false;
          },
          duration: 100
        });
      }*/
      tooltipElm.css({left: (pos.left)+'px', top: (pos.top-height+5) + 'px'});
    });
    
    elm.bind('mouseout', function(e) {
      if($(e.toElement).parents('.calculator-tooltip').length)
        return;
      if(tooltipElm[0].target == elm[0]) {
        setTimeout(function() {
          if(!tooltipElm[0].target && !tooltipElm[0].selftarget) {
            /*tooltipElm[0].animationInProgress = true;
            tooltipElm.fadeOut({
              complete: function() {
                tooltipElm[0].animationInProgress = false;
              },
              duration: 100
            });*/
            tooltipElm.hide();
          }
        }, 200);
      }
      tooltipElm[0].target = null;
    });

    $scope.$watch('tooltip', function(value) {
      if(value) {
        elm[0].tooltip = value;
        elm.addClass('calculator-tooltip');
      }
      else
        elm[0].tooltip = null;
    });
  }

  return {
    restrict: 'A',
    scope: {
      tooltip: '=calculatorTooltip',
      type: '@tooltipType',
      position: '@tooltipPosition'
    },
    link: function($scope, elm, attrs) {
      var tooltipType = $scope.type || 'html';
      switch(tooltipType) {
        case 'simple':
          simpleTooltip($scope, elm);
          break;
        case 'html':
          htmlTooltip($scope, elm);
          break;
      }
    }
  };
}]);

$(document).ready(function() {
  $('body').append('<div id="calculatorTooltip" style="width:250px;z-index:999;position:absolute;padding:10px 0;display:none;"><div class="tooltip-inner" style="background-color:#fff;padding:10px;border:#000 solid 1px;"></div></div>');
  var tooltipElm = $('#calculatorTooltip');
  tooltipElm.bind('mouseover', function() {
    tooltipElm[0].selftarget = true;
  });
  tooltipElm.bind('mouseout', function() {
    tooltipElm[0].selftarget = false;
    tooltipElm[0].target = null;
    setTimeout(function() {
      if(!tooltipElm[0].target && !tooltipElm[0].selftarget)
        /*tooltipElm[0].animationInProgress = true;
        tooltipElm.fadeOut({
          complete: function() {
            tooltipElm[0].animationInProgress = false;
          },
          duration: 100
        });*/
        tooltipElm.hide();
    }, 100)
  });
});
