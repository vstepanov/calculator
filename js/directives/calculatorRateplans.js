angular.module('calculator').directive('calculatorRateplans', ['$state', 'common', 'api', 'components', 'template', function($state, $common, $api, $components, $template) {
  return {
    restrict: 'A',
    templateUrl: $template('calculator-rateplans.html'),
    link: function($scope, elm, attrs) {
      $scope.component = $components.get('rateplans');
      $api.load().then(function(calculator) {
        var collection = calculator.getRateplans(),
            rateplans = collection.items,
            voip = calculator.getVoIP();
            
        switch($state.current.name) {
          case 'calculator.internet':
            rateplans = $common.multipleFilter(rateplans, {group: 'inet'});
            $scope.voip = voip;
            break;
          case 'calculator.tv':
            rateplans = $common.multipleFilter(rateplans, {group: 'dvbc'});
            break;
          case 'calculator.packages':
            rateplans = $common.multipleFilter(rateplans, {group: 'inetdvbc'});
            $scope.voip = voip;
            break;
          case 'activation':
            rateplans = $common.multipleFilter(rateplans, {group: ['inetdvbc', 'dvbc']});
            break;
        }
        
        $scope.rateplans = rateplans;

      });
    }
  };
}]);