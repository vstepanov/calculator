angular.module('calculator').directive('checkboxitemPrice', ['template', function($template) {
  return {
    restrict: 'A',
    scope: {
      item: '=checkboxitemPrice'
    },
    templateUrl: $template('checkboxitem-price.html'),
    link: function($scope, elm, attrs) {
      $scope.update = function() {
        $scope.from = $scope.item.isGroup && !$scope.item.selectedItem;
        $scope.price = $scope.item.selectedItem ? $scope.item.selectedItem.price : $scope.item.price;
      }
      $scope.$watch($scope.item.isGroup ? 'item.selectedItem' : 'item.selected', $scope.update);
      $scope.update();
    }
  };
}]);