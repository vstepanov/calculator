/*
  
*/
angular.module('calculator').directive('calculatorOffer', ['$state', 'template', function($state, $template) {
  return {
    restrict: 'A',
    templateUrl: $template('calculator-offer.html'),
    link: function($scope, elm, attrs) {
      elm.addClass('calculator-offer');
      
    }
  };
}]);