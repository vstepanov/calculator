angular.module('calculator').directive('calculatorRateplan', ['template', function($template) {
  return {
    restrict: 'A',
    scope: {
      rateplan: '=calculatorRateplan'
    },
    templateUrl: $template('calculator-rateplan.html'),
    link: function($scope, elm, attrs) {
    
      elm.addClass('rateplan-wrapper');
      
      $scope.description = $scope.rateplan.description;
      
      // преобразование описания для интернет тарифов
      var matches = $scope.rateplan.description.match(/(\d*)\/(\d*) (.*)/);
      if(matches && matches.length == 4)
        $scope.description = matches[1] + ' ' + matches[3];
        
      $scope.click = function() {
        $scope.rateplan.selected ? $scope.rateplan.deselect() : $scope.rateplan.select();
      };
      
    }
  };
}]);