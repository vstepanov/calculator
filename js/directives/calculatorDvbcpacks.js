angular.module('calculator').directive('calculatorDvbcpacks', ['template', 'components', 'api', function($template, $components, $api) {
  return {
    restrict: 'A',
    scope: {
      expand: '@expand'
    },
    templateUrl: $template('calculator-dvbcpacks.html'),
    link: function($scope, elm, attrs) {
      $scope.component = $components.get('dvbcpacks');
      $api.load().then(function(calculator) {
        var collection = calculator.getDVBCPacks(),
            dvbcpacks = collection.items;
        $scope.dvbcpacks = dvbcpacks;
      });
    }
  };
}]);