angular.module('calculator').directive('calculatorEquipments', ['$state', 'common', 'api', 'components', 'template', function($state, $common, $api, $components, $template) {
  return {
    restrict: 'A',
    scope: {
      expand: '@expand'
    },
    templateUrl: $template('calculator-equipments.html'),
    link: function($scope, elm, attrs) {
      $scope.component = $components.get('equipments');
      $api.load().then(function(calculator) {
        var collection = calculator.getEquipments(),
            equipments = collection.items;
            
        switch($state.current.name) {
          case 'calculator.internet':
            equipments = $common.multipleFilter(equipments, {group: ['inet', 'voip', 'common']});
            break;
          case 'calculator.tv':
            equipments = $common.multipleFilter(equipments, {group: ['dvbc', 'common']});
            break;
          case 'calculator.packages':
            equipments = $common.multipleFilter(equipments, {group: ['inet', 'voip', 'dvbc', 'common']});
            break;
        }
        
        $scope.equipments = equipments;
      });
    }
  };
}]);