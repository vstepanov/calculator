/*
  Эта директива является основой для других директив типа calculatorRateplans, calculatorInetservices и тп 
*/
angular.module('calculator').directive('calculatorBlock', ['template', function($template) {
  return {
    restrict: 'A',
    transclude: true,
    scope: {
      title:'@title',
      expand: '@expand'
    },
    templateUrl: $template('calculator-block.html'),
    link: function($scope, elm, attrs) {
      var component = $scope.$parent.component;
      elm.addClass('calculator-block');
      elm.removeAttr('title');
      $scope.canBeExpanded = typeof $scope.expand != 'undefined' && $scope.expand != 'always';
      $scope.expanded = $scope.expand == 'no' ? false : true;
      if(component)
        $scope.expanded = component.get('expanded', $scope.expanded);
      $scope.switchExpanding = function() {
        $scope.expanded = !$scope.expanded;
        elm.toggleClass('expanded reduced');
        if(component)
          component.set('expanded', $scope.expanded);
      };
      elm.addClass($scope.expanded ? 'expanded' : 'reduced');
    }
  };
}]);