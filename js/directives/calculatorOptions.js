angular.module('calculator').directive('calculatorOptions', ['$state', 'common', 'api', 'components', 'template', function($state, $common, $api, $components, $template) {
  return {
    restrict: 'A',
    scope: {
      expand: '@expand'
    },
    templateUrl: $template('calculator-options.html'),
    link: function($scope, elm, attrs) {
      $scope.component = $components.get('options');
      $api.load().then(function(calculator) {
        var collection = calculator.getOptions(),
            options = collection.items;
        
        switch($state.current.name) {
          case 'calculator.internet':
            options = $common.multipleFilter(options, {group: ['inet', 'voip', 'common']});
            break;
          case 'calculator.tv':
            options = $common.multipleFilter(options, {group: ['dvbc', 'common']});
            break;
          case 'calculator.packages':
            options = $common.multipleFilter(options, {group: ['inet', 'voip', 'dvbc', 'common']});
            break;
        }
        
        $scope.options = options;
      });
    }
  };
}]);