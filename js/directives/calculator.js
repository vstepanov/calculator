/*
  Основная директива, именно она вставляется на страницу.
  Может принимать начальное состояние.
  В зависимости от начального состояния может содержать вкладки.
*/
angular.module('calculator').directive('calculator', ['$state', 'template', 'api', 'error', function($state, $template, $api, $error) {
  return {
    restrict: 'A',
    scope: {
      state: '@calculator',
      contract: '@contract'
    },
    templateUrl: $template('layout.html'),
    link: function ($scope, elm, attrs) {
      
      if($scope.state && !$state.is($scope.state))
        $state.transitionTo($scope.state);
      
      elm.attr('id', 'calculator');
      
      $scope.tabs = [
        {name: 'Интернет',     state: 'calculator.internet'},
        {name: 'ТВ',           state: 'calculator.tv'},
        {name: 'Пакеты услуг', state: 'calculator.packages'}
      ];
      
      $api.load({contract: $scope.contract != 'no'}).then(function() {
        $scope.ready = true;
      });
    }
  };
}]);