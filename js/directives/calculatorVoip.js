angular.module('calculator').directive('calculatorVoip', ['template', 'calculator', function($template, $calculator) {
  return {
    restrict: 'A',
    scope: {},
    template: '<table ng-include="\''+$template('voip/numcategories.html')+'\'" class="voip-numcategories"></table><table ng-include="\''+$template('voip/rateplans.html')+'\'" class="voip-rateplans"></table>',
    link: function($scope, elm, attrs) {
      
      $scope.voip = $calculator.getVoIP();
      
    }
  };
}]);