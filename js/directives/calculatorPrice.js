angular.module('calculator').directive('calculatorPrice', function() {
  return {
    restrict: 'A',
    scope: {
      price: '=calculatorPrice'
    },
    template: '<span ng-bind="price.value" class="value"></span> <span ng-bind="price | pricePeriod" class="period"></span>',
    link: function($scope, elm, attrs) {
      elm.addClass('calculator-price');
    }
  };
});