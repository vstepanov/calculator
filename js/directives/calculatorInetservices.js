angular.module('calculator').directive('calculatorInetservices', ['api', 'components', 'template', function($api, $components, $template) {
  return {
    restrict: 'A',
    scope: {
      expand: '@expand'
    },
    templateUrl: $template('calculator-inetservices.html'),
    link: function($scope, elm, attrs) {
      $scope.component = $components.get('inetservices');
      $api.load().then(function(calculator) {
        var collection = calculator.getInetservices(),
            inetservices = collection.items;
            
        $scope.inetservices = inetservices;
      });
    }
  };
}]);