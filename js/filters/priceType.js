angular.module('calculator').filter('priceType', ['$filter', 'constant', 'common', function($filter, $constant, $common) {
  return function(price, scope) {
    var typeTitle;
    switch(price.type) {
      case $constant.PRICE_TYPE_BUY:
        typeTitle = 'Купить';
        break;
      case $constant.PRICE_TYPE_RENT:
        typeTitle = 'Аренда';
        break;
      case $constant.PRICE_TYPE_CREDIT:
        typeTitle = 'Рассрочка на ' + price.credit_years + ' ' + $common.units(price.credit_years, {nom: 'год', gen: 'года', plu: 'лет'});
        break;
    }
    return typeTitle;
  }
}]);