angular.module('calculator').filter('voipRateplan', ['$sce', function($sce) {
  return function(rate, scope) {
    return typeof rate == 'string' ? $sce.trustAsHtml(rate.replace('|', '<br/>')) : '';
  }
}]);