angular.module('calculator').filter('pricePeriod', ['error', function($error) {
  return function(price, scope) {
    var periodTitle;
    switch(price.period) {
      case 0:
        periodTitle = '';
        break;
      case 1:
        periodTitle = 'мес';
        break;
      case 12:
        periodTitle = 'год';
        break;
      default:
        $error.add('Unknown price period ['+period+']');
    }
    return 'руб.' + (periodTitle ? '/' : '') + periodTitle;
  }
}]);