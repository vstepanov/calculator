angular.module('calculator', [
  'ui.router',
  'ngDialog'
]).
config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
  $stateProvider.
    state('summary', {
      url: '/summary',
      templateUrl: "/calculator/views/summary.html"
    }).
    state('activation', {
      url: '/activation',
      templateUrl: "/calculator/views/activation.html"
    }).
    state('calculator', {
      templateUrl: '/calculator/views/calculator.html'
    }).
    state('calculator.internet', {
      url: '/internet',
      templateUrl: '/calculator/views/calculator/internet.html'
    }).
    state('calculator.tv', {
      url: '/tv',
      templateUrl: '/calculator/views/calculator/tv.html'
    }).
    state('calculator.packages', {
      url: '/packages',
      templateUrl: '/calculator/views/calculator/packages.html'
    });
  $urlRouterProvider.when('', ['$state', function($state) {
    $state.transitionTo('calculator.internet');
  }]);
  $locationProvider.hashPrefix('!');
}])
.run(['$rootScope', 'template', function($rootScope, $template) {
  $rootScope.getTemplateUrl = $template; // передаем в шаблон сервис template в качестве функции getTemplateUrl
}]);